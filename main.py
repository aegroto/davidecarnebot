import telegram
import string
import re
import random

from telegram.ext import Updater, MessageHandler, CommandHandler, Filters 
from init import *

init_logging()
configs = init_configs()
token = init_token()
bot = init_bot(token)

def main():
    updater = Updater(token)
    dispatcher = updater.dispatcher

    message_handler = MessageHandler(Filters.text, message_parse_handler)
    dispatcher.add_handler(message_handler)

    principessa_handler = CommandHandler('principessa', principessa_cmd_handler)
    dispatcher.add_handler(principessa_handler)

    ddh_handler = CommandHandler('ddh', ddh_cmd_handler)
    dispatcher.add_handler(ddh_handler)

    donzelli_handler = CommandHandler('donzelli', donzelli_cmd_handler)
    dispatcher.add_handler(donzelli_handler)

    donzelle_handler = CommandHandler('donzelle', donzelle_cmd_handler)
    dispatcher.add_handler(donzelle_handler)

    updater.start_polling()

def principessa_cmd_handler(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text="Ma sei una principessa!")

def ddh_cmd_handler(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text="Ddddddddhhh!")

def donzelli_cmd_handler(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text="Non conquistare troppi donzelli")

def donzelle_cmd_handler(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text="Non conquistare troppe donzelle")

def message_parse_handler(bot, update):
    reply = get_message_reply(update.message.text)
    if reply is not None:
        update.message.reply_text(reply, reply_to_message_id=update.message.message_id, parse_mode="markdown")

def get_message_reply(msg_text):
    msg_text.translate(string.punctuation)
    msg_symbols = msg_text.split()

    message_triggers = configs["message_triggers"]
    trigger_replies = configs["trigger_replies"]

    for symbol in msg_symbols:
        for trigger_id in message_triggers:
            for trigger_symbol in message_triggers[trigger_id]:
                if symbol.lower() == trigger_symbol:
                    return random.choice(trigger_replies[trigger_id])

    if(random.uniform(0.0, 1.0) > (1.0 - configs["random_reply_chance"])):
        return random.choice(trigger_replies["random"])

    return None

if __name__ == '__main__':
    main()