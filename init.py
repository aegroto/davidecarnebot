import telegram, yaml
import logging

def init_logging():
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

def init_configs():
    configs = yaml.load(open("config/settings.yaml"))
    return configs

def init_token():
    token_file = yaml.load(open("config/token.yaml"))
    return token_file["token"]

def init_bot(token):
    bot = telegram.Bot(token)